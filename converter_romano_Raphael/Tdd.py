import unittest
from Desafio_romano import valida_numero
from Desafio_romano import numero_para_romano



class TestStringMethods(unittest.TestCase):

    def test_validacao(self):
        self.assertEqual(
            valida_numero("1"),True)
        self.assertEqual(
            valida_numero(5), True)
        self.assertEqual(
            valida_numero("123qwe"),False)
        self.assertEqual(
            valida_numero(5000),False
        )
        self.assertEqual(
            valida_numero("-10"),False
        )
        self.assertEqual(
            valida_numero(2490), True
        )

    def test_numero(self):
        self.assertEqual(
            numero_para_romano(20),"XX"
        )
        self.assertEqual(
            numero_para_romano(4000), False
        )
        self.assertEqual(
            numero_para_romano("10"), "X"
        )
        self.assertEqual(
            numero_para_romano("-10"), False
        )
        self.assertEqual(
            numero_para_romano(3958),"MMMCMLVIII"
        )

def runTests():
    suite = unittest.defaultTestLoader.loadTestsFromTestCase(TestStringMethods)
    unittest.TextTestRunner(verbosity=2, failfast=True).run(suite)

runTests()


def valida_numero(numero):
    try:
        numero = int(numero)
    except:
        return False
    if not (0 < numero < 3999):
        return False
    return True

def numero_para_romano(numero):
    if valida_numero(numero) == True:
        numero = int(numero)
        if numero < 10:
            return (unidade(numero))
        elif numero< 100:
            return (decimal(numero//10)+unidade(numero%10))
        elif numero<1000:
            var = (numero//100) * 100
            return (centena(numero//100)+decimal((numero-var)//10) + unidade((numero-var)%10))
        #tratamento diferente para numeros > 1000, sendo possivel usar para numeros maiores que 4000 caso precise
        else :
            cont = 1
            mil = ""
            while cont <= numero//1000:
                mil = mil+"M"
                cont += 1
            return ( mil+ centena(numero//100%10)+decimal(numero//10%10)+unidade(numero%10))
    else :
        return False



def unidade(uni):
    primeiro = {0: "", 1: "I", 2: "II", 3: "III", 4: "IV", 5: "V", 6: "VI", 7: "VII", 8: "VIII", 9: "IX"}
    return primeiro[uni]

def decimal(dez):
    segundo = {0: "", 1: "X", 2: "XX", 3: "XXX", 4: "XL", 5: "L", 6: "LX", 7: "LXX", 8: "LXXX", 9: "XC"}
    return segundo[dez]

def centena(cen):
    terceiro = {0: "", 1: "C", 2: "CC", 3: "CCC", 4: "CD", 5: "D", 6:"DC", 7: "DCC", 8: "DCCC", 9: "CM"}
    return terceiro[cen]

